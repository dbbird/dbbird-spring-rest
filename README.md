# Projeto Bem-Te-Vi

#### 1 - Entendendo o projeto



O Projeto Bem-Te-Vi é uma aplicação REST para Ornitólogos amadores interessados em aves. Nela os ornitólogos podem buscar por aves no guia e registrar o avistamento das mesmas. O guia tem varias informações sobre os pássaros buscados.



#### 2 - Executando localmente



###### 2.1 - Pre-requisitos

Primeiro certifique-se de ter instalado as seguintes ferramentas: 

* `git` - https://docs.github.com/en/get-started/quickstart/set-up-git
* `JDK`  - https://www.oracle.com/br/java/technologies/downloads/
* `Gradle` - https://gradle.org/install/
* Your prefered IDE
  * Eclipse - https://projects.eclipse.org/projects/technology.m2e
  * Spring Tools Suite - https://spring.io/tools
  * IntelliJ IDEA - https://www.jetbrains.com/idea/download/
  * VS Code - https://code.visualstudio.com/download



###### 2.2 - Rodando o backend

You can run the application locally following the steps:

`git clone git@gitlab.com:dbbird/bemtevi-project-rest.git`

`cd bemtevi-project-rest`

`./gradlew bootRun`

![Terminal](https://i.imgur.com/Wz92wWP.png)



Apos rodar esses comandos no terminal você terá acesso a aplicação pelo navegador no link: http://localhost:8080/api/birds

![API](https://i.imgur.com/yT9J3oN.png)

A documentação da API também podera ser acessada em: http://localhost:8080/swagger-ui.html

![API Documentation](https://i.imgur.com/oXCKNEo.png)



###### 2.3 - Configuração do Banco de Dados

Bem-Te-Vi project uses an in-memory database (H2) which gets populated at startup with data.

#### 3 - Funcionalidades

- [x]  Adicionar novo pássaro
- [x]  Listar pássaros
- [x]  Buscar pássaro por id
- [x]  Buscar pássaro por nome
- [x]  Atualizar pássaro
- [x]  Deletar pássaro
- [x]  Adicionar avistamento
- [x]  Listar avistamentos
- [x]  Listar avistamentos do pássaro
- [x]  Buscar avistamento por id
- [ ]  Atualizar avistamento
- [ ]  Deletar avistamento
- [ ]  Adicionar ornitólogo 
- [ ]  Listar ornitólogos
- [ ]  Atualizar ornitólogo
- [ ]  Deletar ornitólogo



#### 5 - Como contribuir

1.  Faça um **fork** do projeto
2. Crie uma nova branch com as suas alterações: `git checkout -b my-feature`
3. Salve as alterações e crie uma mensagem de commit contando o que voçê fez: `git commit -m "feature: my new feature"`
4. Envie as suas alterações: `git push origin my-feature`
5. Abra um Pull Request



#### 6 - Licença



Este projeto esta sob a licença [MIT](https://gitlab.com/dbbird/bemtevi-project-rest/-/blob/main/LICENSE)



