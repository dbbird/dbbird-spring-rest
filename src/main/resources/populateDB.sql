INSERT INTO bird (portuguese_name,english_name,scientific_name,predominant_color,family,habitat,genus,size,image) 
            VALUES ('corvo-comum','raven','corvus corax','black','corvidae','bosques','corvus',56,'https://upload.wikimedia.org/wikipedia/commons/9/91/Corvus_corax_clarionensis_perched_frontal.jpg');

INSERT INTO bird (portuguese_name,english_name,scientific_name,predominant_color,family,habitat,genus,size,image)
            VALUES ('papagaio-verdadeiro','true parrot','amazona aestiva','green','psittacidae','florestas','amazona',37,'https://upload.wikimedia.org/wikipedia/commons/6/6d/Blue-and-Yellow-Macaw.jpg');

INSERT INTO bird (portuguese_name,english_name,scientific_name,predominant_color,family,habitat,genus,size,image)
            VALUES('carcara','crested caracara','caracara plancus','marrom','falconidae','campos abertos','caracara',60,'https://upload.wikimedia.org/wikipedia/commons/a/ad/Schopfkarakara.jpg');

INSERT INTO sighting (date,description,bird)
            VALUES('2018-01-01','primeira observação',0);

INSERT INTO sighting (date,description,bird)
            VALUES('2018-01-02','segunda observação',0);

INSERT INTO sighting (date,description,bird)
            VALUES('2018-01-03','primeira observação',1);

INSERT INTO sighting (date,description,bird)
            VALUES('2018-01-04','primeira observação',2);