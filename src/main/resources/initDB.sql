DROP TABLE IF EXISTS bird;
DROP TABLE IF EXISTS sighting;

CREATE TABLE bird (

    id INTEGER IDENTITY PRIMARY KEY,
    portuguese_name VARCHAR(30),
    english_name VARCHAR(30), 
    scientific_name VARCHAR(30),
    predominant_color VARCHAR(30),
    family VARCHAR(30),
    habitat VARCHAR(30),
    genus VARCHAR(30),
    size DECIMAL,
    image VARCHAR(255),
);

CREATE TABLE sighting (

    id INTEGER IDENTITY PRIMARY KEY,
    date VARCHAR(30),
    description VARCHAR(30),
    bird INTEGER NOT NULL,
);
ALTER TABLE sighting ADD CONSTRAINT fk_sigthing_bird FOREIGN KEY (bird) REFERENCES bird(id);