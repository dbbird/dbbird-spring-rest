package dbbird.bemteviprojectrest.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.repository.Repository;

import dbbird.bemteviprojectrest.model.Sighting;

/**
 * Interface {@code SightingRepository} is a repository that contains all the
 * bird sightings.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
public interface SightingRepository extends Repository<Sighting, Long> {

    /**
     * Save a sighting in the repository. 
     * @param sighting the Sighting to be saved
     */
    void save(Sighting sighting);

    /**
     * Retrieve a sighting from the repository by bird id.
     * 
     * @param birdId
     */
    List<Sighting> findByBirdId(Long birdId);

    /**
     * Retrieve a sighting from the repository by its id.
     * 
     * @param id
     * @return a sighting
     */
    Sighting findById(long id);

    /**
     * Retrieve all the birds from the repository.
     * 
     * @return a collection of birds (or an empty collection if no birds could be
     *         found)
     */
    Collection<Sighting> findAll();

    /**
     * Delete a sighting from the repository.
     * @param sighting
     */
    void delete(Sighting sighting);
}
