package dbbird.bemteviprojectrest.repository;

import java.util.Collection;
import org.springframework.data.repository.Repository;

import dbbird.bemteviprojectrest.model.Bird;

/**
 * Interface {@code BirdRepository} is a repository that contains all the birds.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
public interface BirdRepository extends Repository<Bird, Long> {

    /**
     * Retrieve a bird from the repository by its id.
     * 
     * @param id
     * @return a bird
     */
    Bird findById(long id);

    /**
     * Retrieve a bird from the repository by its portuguese name.
     * 
     * @param portugueseName
     * @return a bird
     */
    Bird findByPortugueseName(String portugueseName);
    
    /**
     * Retrieve all the birds from the repository.
     * 
     * @return a collection of birds (or an empty collection if no birds could be found)
     */
    Collection<Bird> findAll();    

    /**
     * Save a Bird in Database.
     * 
     * @param bird
     */
    void save(Bird bird);

    /**
     * Delete a Bird in Database.
     * 
     * @param id
     */
    Bird deleteById(long id);
}
