package dbbird.bemteviprojectrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Bem-te-vi Project is a REST API to help people to identify birds in your observation.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 * 
 */

@SpringBootApplication
@EnableSwagger2
public class BemteviProjectRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BemteviProjectRestApplication.class, args);
	}

}
