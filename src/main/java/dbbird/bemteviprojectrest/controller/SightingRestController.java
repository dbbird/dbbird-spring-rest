package dbbird.bemteviprojectrest.controller;

import java.util.logging.Logger;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dbbird.bemteviprojectrest.model.Sighting;
import dbbird.bemteviprojectrest.repository.SightingRepository;

/**
 * {@code SightingRestController} retrieves the information about the sightings.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/sightings")
public class SightingRestController {

    private Logger logger = Logger.getLogger(SightingRestController.class.getCanonicalName());

    @Autowired
    private SightingRepository sightingRepository;

    /**
     * Get all sightings.
     * 
     * @return a collection of sightings,
     *         as HTTP status,
     *         if the sightings were found, 200 (OK),
     *         if the sightings were not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Sighting>> getAllSightings() {
        Collection<Sighting> sightings = this.sightingRepository.findAll();
        if (sightings == null) {
            logger.info("Sightings not found");
            return new ResponseEntity<Collection<Sighting>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Collection<Sighting>>(sightings, HttpStatus.OK);
    }

    /**
     * Gets a sighting by its id.
     * 
     * @param id
     * @return a sighting,
     *         as HTTP status,
     *         if the sighting was found, 200 (OK),
     *         if the sighting was not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Sighting> getSighting(@PathVariable("id") long id) {
        logger.info("Fetching Sighting with id " + id);
        Sighting sighting = this.sightingRepository.findById(id);
        if (sighting == null) {
            logger.info("Sighting with id " + id + " not found");
            return new ResponseEntity<Sighting>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Sighting>(sighting, HttpStatus.OK);
    }

    /**
     * Get sighting by bird id.
     * 
     * @return a Sighting,
     *         as HTTP status,
     *         if the sighting was found, 200 (OK),
     *         if the sighting was not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */
    @RequestMapping(value = "/bird/{birdId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Sighting>> getSightingByBirdId(@PathVariable("birdId") long birdId) {
        logger.info("Fetching Sighting with bird id " + birdId);
        Collection<Sighting> sightings = sightingRepository.findByBirdId(birdId);
        if (sightings == null) {
            logger.info("Sighting with bird id " + birdId + " not found");
            return new ResponseEntity<Collection<Sighting>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Collection<Sighting>>(sightings, HttpStatus.OK);
    }

    /**
     * Add a sighting.
     * 
     * @param sighting
     * @return a sighting,
     *       as HTTP status,
     *      if the sighting was added, 201 (CREATED),
     *     if the sighting was not added, 404 (NOT FOUND).
     *    if bad request, 500.
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Sighting> addSighting(@RequestBody Sighting sighting) {
        logger.info("Adding Sighting " + sighting);
        if (sighting == null) {
            logger.info("Sighting not added");
            return new ResponseEntity<Sighting>(HttpStatus.BAD_REQUEST);
        }
        this.sightingRepository.save(sighting);
        return new ResponseEntity<Sighting>(sighting, HttpStatus.CREATED);
    }
    


    
}
