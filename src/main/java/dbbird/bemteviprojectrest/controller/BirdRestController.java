package dbbird.bemteviprojectrest.controller;

import java.util.logging.Logger;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dbbird.bemteviprojectrest.model.Bird;
import dbbird.bemteviprojectrest.repository.BirdRepository;

/**
 * {@code BirdRestController} retrieves the information about the birds.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
@RestController
@CrossOrigin(exposedHeaders = "errors, content-type")
@RequestMapping("api/birds")
public class BirdRestController {

    private Logger logger = Logger.getLogger(BirdRestController.class.getCanonicalName());

    @Autowired
    private BirdRepository birdRepository;

    /**
     * Gets a bird by its id.
     * 
     * @param id
     * @return a bird,
     *         as HTTP status,
     *         if the bird was found, 200 (OK),
     *         if the bird was not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Bird> getBird(@PathVariable("id") long id) {
        logger.info("Fetching Bird with id " + id);
        Bird bird = birdRepository.findById(id);
        if (bird == null) {
            logger.info("Bird with id " + id + " not found");
            return new ResponseEntity<Bird>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Bird>(bird, HttpStatus.OK);
    }

    /**
     * Get bird by name.
     * 
     * @return a Bird,
     *         as HTTP status,
     *         if the birds were found, 200 (OK),
     *         if the birds were not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */

    @RequestMapping(value = "/bird/name", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Bird> getBird(@RequestParam(value = "name") String name) {
        logger.info("Fetching Bird with name " + name);
        Bird bird = birdRepository.findByPortugueseName(name);
        if (bird == null) {
            logger.info("Bird with name " + name + " not found");
            return new ResponseEntity<Bird>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Bird>(bird, HttpStatus.OK);
    }

    /**
     * Gets all birds in the repository.
     * 
     * @return a collection of birds,
     *         as HTTP status,
     *         if the birds were found, 200 (OK),
     *         if the birds were not found, 404 (NOT FOUND).
     *         if bad request, 500.
     */
    @RequestMapping(value = "", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Bird>> getAllBirds() {
        Collection<Bird> bird = this.birdRepository.findAll();
        logger.info("getAllBirds() found: " + bird);
        if (bird == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(bird, HttpStatus.OK);
    }

    /**
     * Save Bird in Database
     * 
     * @param newBird
     * 
     */
    @RequestMapping(value = "/bird", method = RequestMethod.POST)
    public ResponseEntity<Bird> createBird(@RequestBody Bird newBird){
        
        if(newBird == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        this.birdRepository.save(newBird);
        
        return new ResponseEntity<>(newBird, HttpStatus.CREATED);
    }


    /**
     * Update Bird in Database
     * 
     * @param newBird
     * 
     */
    @RequestMapping(value = "/bird/{id}/edit", method = RequestMethod.PUT)
    public ResponseEntity<Bird> updateBird(@PathVariable("id") long id, @RequestBody Bird newDataBird){

        Bird bird = this.birdRepository.findById(id);

        if(bird == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        this.birdRepository.save(newDataBird);
        
        return new ResponseEntity<>(newDataBird, HttpStatus.ACCEPTED);
    }    


    /**
     * Delete Bird in Database
     * 
     * @param newBird
     * 
     */
    @RequestMapping(value = "/bird/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Bird> deleteBirdByID(@PathVariable("id") long id){

        Bird bird = this.birdRepository.findById(id);

        if(bird == null)
            return new ResponseEntity<Bird>(HttpStatus.NOT_FOUND);
        
        
        if (bird.getId() == id) 
            this.birdRepository.deleteById(id);
            
        return new ResponseEntity<Bird>(bird, HttpStatus.OK);        
        
    }        


       


}
