package dbbird.bemteviprojectrest.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Sighting is a class that represents a bird sighting.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
@Entity
public class Sighting {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * Holds value of property date.
     */
    private String date;

    /**
     * Holds value of property description.
     */
    private String description;

    /**
     * Holds value of property bird.
     */
    @ManyToOne
    @JoinColumn(name = "bird")
    private Bird bird;

     /**
     * Creates a new instance of Sighting for the current date
     * 
     * @param date
     * @param description
     * @param bird
     */
    public Sighting() {
       
    } 

    protected Sighting(String date, String description, Bird bird) {
        this.date = date;
        this.description = description;
        this.bird = bird;
    }

    /**
     * Getter for Sightings id.
     * 
     * @return the id of the sighting.
     */
    public long getId() {
        return id;
    }

    /**
     * Setter for Sightings id.
     * 
     * @param id the id of the sighting.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for property date.
     *
     * @return Value of property date.
     */
    public String getDate() {
        return date;
    }

    /**
     * Setter for property date.
     *
     * @param date New value of property date.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Getter for property description.
     *
     * @return Value of property description.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for property description.
     *
     * @param description New value of property description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for property pet.
     *
     * @return Value of property pet.
     */
    public Bird getBird() {
        return this.bird;
    }

    /**
     * Setter for property pet.
     *
     * @param pet New value of property pet.
     */
    public void setBird(Bird bird) {
        this.bird = bird;
    }

}