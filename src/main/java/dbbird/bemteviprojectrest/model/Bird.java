package dbbird.bemteviprojectrest.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Bird is a class that represents a bird.
 * 
 * @author Rayane Paiva
 * @author Roger Foss
 */
@Entity
public class Bird {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    private String portugueseName;
    private String englishName;
    private String scientificName; 
    private String predominantColor;
    private String family;
    private Double size;
    private String habitat;
    private String genus;
    private String image;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bird")
    private Set<Sighting> sightings;

    /**
     * Creates a new {@code Bird} that contains all the information about a bird.
     * 
     * @param portugueseName
     * @param englishName
     * @param scientificName
     * @param predominantColor
     * @param family
     * @param habitat
     * @param genus
     * @param size
     * @param image
     */
    public Bird(String portugueseName, String englishName, String scientificName, String predominantColor, String family, String habitat, String genus, Double size, String image) {
        this.portugueseName = portugueseName;
        this.englishName = englishName;
        this.scientificName = scientificName;
        this.predominantColor = predominantColor;
        this.family = family;
        this.size = size;
        this.habitat = habitat;
        this.genus = genus;
        this.image = image;
    }

    /**
     * 
     */
    protected Bird() {
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the portugueseName
     */
    public String getPortugueseName() {
        return portugueseName;
    }

    /**
     * @return the englishName
     */
    public String getEnglishName() {
        return englishName;
    }        

    /**
     * @return the scientificName
     */
    public String getScientificName() {
        return scientificName;
    }

    /**
     * @return the predominantColor
     */
    public String getpredominantColor() {
        return predominantColor;
    }

    /**
     * @return the family
     */
    public String getFamily() {
        return family;
    }

    /**
     * @return the Habitat
     */
    public String getHabitat() {
        return habitat;
    }

    /**
     * @return the Genus
     */
    public String getGenus() {
        return genus;
    }

    /**
     * @return the Size
     */
    public Double getSize() {
        return size;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Bird [id=" + id + ", portugueseName=" + portugueseName + ", englishName=" + englishName
                + ", scientificName=" + scientificName + ", predominantColor=" + predominantColor + ", family=" + family
                + ", size=" + size + ", habitat=" + habitat + ", genus=" + genus + ", image=" + image + "]";
    }
    
}