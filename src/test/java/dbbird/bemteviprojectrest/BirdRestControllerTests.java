package dbbird.bemteviprojectrest;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import dbbird.bemteviprojectrest.controller.BirdRestController;
import dbbird.bemteviprojectrest.model.Bird;
import dbbird.bemteviprojectrest.repository.BirdRepository;

/**
 * {@code BirdRestControllerTests} is a test class for {@code BirdRestController}.
 * 
 * @author Rayane Paiva
 * @author Roger Foss 
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class BirdRestControllerTests {

    @Autowired
    private BirdRestController birdRestController;
    
    @MockBean
    private BirdRepository birdRepository;

    private MockMvc mockMvc;

    private List<Bird> birds;

    @Before
    public void initBirds(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(birdRestController).build();
        birds = new ArrayList<Bird>();

        Bird bird = new Bird("corvo-comum", "raven", "corvus corax", "black", "corvidae","bosques","corvus", 56.0, "https://upload.wikimedia.org/wikipedia/commons/9/91/Corvus_corax_clarionensis_perched_frontal.jpg");        
        birds.add(bird);

        bird = new Bird("papagaio-verdadeiro","true parrot","amazona aestiva", "green","psittacidae", "florestas","amazona",37.0, "https://upload.wikimedia.org/wikipedia/commons/6/6d/Blue-and-Yellow-Macaw.jpg");        
        birds.add(bird);
    }

    @Test
    public void testGetBirdSuccess() throws Exception {
        given(this.birdRepository.findById(0)).willReturn(birds.get(0));
        this.mockMvc.perform(get("/api/birds/0")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.portugueseName").value("corvo-comum"))
            .andExpect(jsonPath("$.englishName").value("raven"))
            .andExpect(jsonPath("$.scientificName").value("corvus corax"))
            .andExpect(jsonPath("$.predominantColor").value("black"))
            .andExpect(jsonPath("$.family").value("corvidae"))
            .andExpect(jsonPath("$.habitat").value("bosques"))
            .andExpect(jsonPath("$.genus").value("corvus"))
            .andExpect(jsonPath("$.size").value(56.0))
            .andExpect(jsonPath("$.image").value("https://upload.wikimedia.org/wikipedia/commons/9/91/Corvus_corax_clarionensis_perched_frontal.jpg"));
    }

    @Test
    public void testGetBirdByNameSuccess() throws Exception {
        given(this.birdRepository.findByPortugueseName("corvo-comum")).willReturn(birds.get(0));
        this.mockMvc.perform(get("/api/birds/name?name=corvo-comum")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.portugueseName").value("corvo-comum"))
            .andExpect(jsonPath("$.englishName").value("raven"))
            .andExpect(jsonPath("$.scientificName").value("corvus corax"))
            .andExpect(jsonPath("$.predominantColor").value("black"))
            .andExpect(jsonPath("$.family").value("corvidae"))
            .andExpect(jsonPath("$.habitat").value("bosques"))
            .andExpect(jsonPath("$.genus").value("corvus"))
            .andExpect(jsonPath("$.size").value(56.0))
            .andExpect(jsonPath("$.image").value("https://upload.wikimedia.org/wikipedia/commons/9/91/Corvus_corax_clarionensis_perched_frontal.jpg"));
    }    

    @Test
    public void testGetBirdNotFound() throws Exception {
        given(this.birdRepository.findById(-1)).willReturn(null);
        this.mockMvc.perform(get("/api/birds/-1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllBirdsSuccess() throws Exception {
        given(this.birdRepository.findAll()).willReturn(birds);
        this.mockMvc.perform(get("/api/birds")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void testGetAllBirdsNotFound() throws Exception {
        birds.clear();
        given(this.birdRepository.findAll()).willReturn(null);
        this.mockMvc.perform(get("/api/birds")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteBirdSuccess() throws Exception {
        
        
    }    
}
