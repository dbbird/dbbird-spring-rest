package dbbird.bemteviprojectrest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dbbird.bemteviprojectrest.model.Bird;
import dbbird.bemteviprojectrest.repository.BirdRepository;

/**
 * {@code BirdRepositoryTests} is a test class for {@code BirdRepository}.
 * 
 * @author Rayane Paiva
 * @author Roger Foss 
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class BirdRepositoryTests {

	@Autowired
	private BirdRepository birdRepository;
	
	@Test
	public void shouldFindAllBirds() {
		assertThat(birdRepository.findAll()).isNotEmpty();
		assertThat(birdRepository.findAll()).hasSize(3);
	}

	@Test
	public void shouldFindBirdById() {
		long id = 0;
		Bird bird = this.birdRepository.findById(id);
		assertThat(bird).isNotNull();
		assertThat(bird.getPortugueseName()).isEqualTo("corvo-comum");
		assertThat(bird.getEnglishName()).isEqualTo("raven");
		assertThat(bird.getScientificName()).isEqualTo("corvus corax");
		assertThat(bird.getpredominantColor()).isEqualTo("black");
		assertThat(bird.getFamily()).isEqualTo("corvidae");
		assertThat(bird.getHabitat()).isEqualTo("bosques");
		assertThat(bird.getGenus()).isEqualTo("corvus");
		assertThat(bird.getSize()).isEqualTo(56);
	}

	@Test
	public void shouldFindBirdByPortugueseName() {
		String portugueseName = "corvo-comum";
		Bird bird = this.birdRepository.findByPortugueseName(portugueseName);
		assertThat(bird).isNotNull();
		assertThat(bird.getId()).isEqualTo(0);
		assertThat(bird.getEnglishName()).isEqualTo("raven");
		assertThat(bird.getScientificName()).isEqualTo("corvus corax");
		assertThat(bird.getpredominantColor()).isEqualTo("black");
		assertThat(bird.getFamily()).isEqualTo("corvidae");
		assertThat(bird.getHabitat()).isEqualTo("bosques");
		assertThat(bird.getGenus()).isEqualTo("corvus");
		assertThat(bird.getSize()).isEqualTo(56);
	}

	@Test
	public void shouldSaveNewBird(){
		
		Bird bird = new Bird("gaviao-real", "harpy eagle", "harpia harpyja", "cinza escuro", "accipitridae", "florestas tropicais", "harpia", 220.0, "https://pt.wikipedia.org/wiki/Gavi%C3%A3o-real#/media/Ficheiro:Harpia_harpyja_001_800.jpg");

		this.birdRepository.save(bird);
		
		bird = this.birdRepository.findByPortugueseName("gaviao-real");

		assertThat(bird).isNotNull();
		assertThat(bird.getPortugueseName()).isEqualTo("gaviao-real");
		assertThat(bird.getEnglishName()).isEqualTo("harpy eagle");
		assertThat(bird.getScientificName()).isEqualTo("harpia harpyja");
		assertThat(bird.getpredominantColor()).isEqualTo("cinza escuro");
		assertThat(bird.getFamily()).isEqualTo("accipitridae");
		assertThat(bird.getHabitat()).isEqualTo("florestas tropicais");
		assertThat(bird.getGenus()).isEqualTo("harpia");
		assertThat(bird.getSize()).isEqualTo(220.0);
		assertThat(bird.getImage()).isEqualTo("https://pt.wikipedia.org/wiki/Gavi%C3%A3o-real#/media/Ficheiro:Harpia_harpyja_001_800.jpg");
		
	}
}
