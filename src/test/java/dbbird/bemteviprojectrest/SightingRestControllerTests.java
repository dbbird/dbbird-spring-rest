package dbbird.bemteviprojectrest;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import dbbird.bemteviprojectrest.model.Sighting;
import dbbird.bemteviprojectrest.controller.SightingRestController;
import dbbird.bemteviprojectrest.model.Bird;
import dbbird.bemteviprojectrest.repository.SightingRepository;

/**
 * {@code SightingRestControllerTests} is a test class for {@code SightingRestController}.
 * 
 * @author Rayane Paiva
 * @author Roger Foss 
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class SightingRestControllerTests {

    @Autowired
    private SightingRestController sightingRestController;

    @MockBean
    private SightingRepository sightingRepository;

    private MockMvc mockMvc;

    private List<Sighting> sightings;

    @Before
    public void initSightings(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(sightingRestController).build();

        sightings = new ArrayList<Sighting>();

        Bird bird = new Bird("corvo-comum", "raven", "corvus corax", "black", "corvidae","bosques","corvus", 56.0, "https://upload.wikimedia.org/wikipedia/commons/9/91/Corvus_corax_clarionensis_perched_frontal.jpg");

        Sighting sighting = new Sighting();
        sighting.setBird(bird);
        sighting.setId(2);
        sighting.setDate("2022-01-01");
        sighting.setDescription("Visita 1. Teste RestController");
        sightings.add(sighting);

        sighting = new Sighting();
        sighting.setBird(bird);
        sighting.setId(3);
        sighting.setDate("2022-01-02");
        sighting.setDescription("Visita 2. Teste RestController");
        sightings.add(sighting);
    }

    @Test
    public void testGetSightingSucess() throws Exception {
        given(this.sightingRepository.findById(2)).willReturn(sightings.get(0));
        this.mockMvc.perform(get("/api/sightings/2")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(2))
            .andExpect(jsonPath("$.description").value("Visita 1. Teste RestController"));
    }

    @Test
    public void testGetSightingNotFound() throws Exception {
        given(this.sightingRepository.findById(-1)).willReturn(null);
        this.mockMvc.perform(get("/api/sightings/-1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllSightingsSucess() throws Exception {
        given(this.sightingRepository.findAll()).willReturn(sightings);
        this.mockMvc.perform(get("/api/sightings")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].id").value(2))
            .andExpect(jsonPath("$[0].description").value("Visita 1. Teste RestController"))
            .andExpect(jsonPath("$[1].id").value(3))
            .andExpect(jsonPath("$[1].description").value("Visita 2. Teste RestController"));
    }

    @Test
    public void testGetAllSightingsNotFound() throws Exception {
        sightings.clear();
        given(this.sightingRepository.findAll()).willReturn(null);
        this.mockMvc.perform(get("/api/sightings")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateSightingSuccess() throws Exception {
        Sighting newSighting = sightings.get(0);
        newSighting.setId(999);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(newSighting);
        this.mockMvc.perform(post("/api/sightings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isCreated());
    }

    @Test(expected = NullPointerException.class)
    public void testCreateSightingError() throws Exception {
        Sighting newSighting = sightings.get(0);
        newSighting.setId(null);
        newSighting.setBird(null);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(newSighting);
        this.mockMvc.perform(post("/api/sightings")
            .contentType(MediaType.APPLICATION_JSON)
            .content(json))
            .andExpect(status().isBadRequest());
        
    }

}