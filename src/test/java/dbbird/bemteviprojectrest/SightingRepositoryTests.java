package dbbird.bemteviprojectrest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dbbird.bemteviprojectrest.model.Sighting;
import dbbird.bemteviprojectrest.repository.SightingRepository;

/**
 * {@code SightingRepositoryTests} is a test class for {@code SightingRepository}.
 * 
 * @author Rayane Paiva
 * @author Roger Foss 
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SightingRepositoryTests {

	@Autowired
	private SightingRepository sightingRepository;

    @Test
    public void shouldFindSightingById(){
        Sighting sighting = sightingRepository.findById(0);
        assertThat(sighting).isNotNull();
        assertThat(sighting.getBird().getPortugueseName()).isEqualTo("corvo-comum");
        assertThat(sighting.getDate()).isEqualTo("2018-01-01");
    }

    @Test
    public void shouldFindSightingByBirdId(){
        Collection<Sighting> sightings = sightingRepository.findByBirdId(0L);
        assertThat(sightings.size()).isEqualTo(2);        
    }

    @Test
    public void shouldFindAllSightings(){
        assertThat(sightingRepository.findAll()).isNotEmpty();
        assertThat(sightingRepository.findAll()).hasSize(4);
    }
}